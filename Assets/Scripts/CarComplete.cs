using UnityEngine;
using System.Collections;

public class CarComplete : MonoBehaviour
{
	//Colliders, these are the actual wheels that make the car move.
	public WheelCollider ColliderLF;
	public WheelCollider ColliderRF;
	public WheelCollider ColliderLB;
	public WheelCollider ColliderRB;
	//These are just fancy looks, we need to make them turn and rotate
	public Transform WheelLF;
	public Transform WheelRF;
	public Transform WheelLB;
	public Transform WheelRB;
	//you want to boost your vehicle or make it unstable, change these variables
	private const float MAX_STEER = 30; 		//[degrees]
	private const float MAX_TORQUE = 200; 		//[Nm]
	//Used for debugging and showing progress to the teacher
	public bool eulerTesting = true;
	public bool combined = false;
	public float wheelBaseLength;
	public float wheelBaseWidth;
	// Use this for initialization
	void Start ()
	{
		//to keep the car from flipping lower the center of mass
		rigidbody.centerOfMass -= Vector3.up * 0.5f;
		wheelBaseWidth = Vector3.Distance (WheelLF.position, WheelRF.position);
		wheelBaseLength = Vector3.Distance (WheelLF.position, WheelLB.position);
	}
	
	// Update is called once per frame
	void Update ()
	{
		// Visual debuging using DrawRay and DrawLine, thank me later
		Debug.DrawRay (transform.position + transform.up, transform.forward, Color.blue);
		Debug.DrawRay (transform.position + transform.up, transform.up, Color.green);
		Debug.DrawRay (transform.position + transform.up, Vector3.Cross (transform.up, transform.forward), Color.red);

		//input from keyboard / joystick
		MoveForwardReverse (Input.GetAxis ("Vertical") * MAX_TORQUE);
		Turn (Input.GetAxis ("Horizontal") * MAX_STEER);

		// [RPS] = [RPM] / 60	 [°/s] = 360 / [RPS],  ° =  [°/s] * [s]
		float angle = (ColliderLB.rpm + ColliderRB.rpm + ColliderLF.rpm + ColliderRF.rpm) / 4 * 6 * Time.deltaTime;
		if (eulerTesting) {
			EulerRotations (angle, ColliderLF.steerAngle, ColliderRF.steerAngle);
		} else {
			QuaternionsRotations (angle, ColliderLF.steerAngle, ColliderRF.steerAngle);
		}

	}


	//Puts negative or positive torque on the car to move the car
	private void MoveForwardReverse (float moveValue)
	{
		if (moveValue > 0) {
			ColliderLB.motorTorque = moveValue;
			ColliderRB.motorTorque = moveValue;
		} else if (moveValue < 0) {
			ColliderLB.motorTorque = moveValue;
			ColliderRB.motorTorque = moveValue;
		} else {
			ColliderRB.motorTorque = 0;
			ColliderLB.motorTorque = 0;
		}
	
	}

	private void Turn (float alpha)
	{
		float leftAngle = 0;
		float rightAngle = 0;
		if (alpha != 0) {
			//calculate the ackerman angles for each wheel
			//http://i258.photobucket.com/albums/hh264/fjcamper/Turncenter.gif
			float orientation = Mathf.Abs (alpha) / alpha;
			float x = Mathf.Abs (wheelBaseLength / Mathf.Tan (Mathf.Deg2Rad * alpha));

			float xc = wheelBaseLength / Mathf.Tan (Mathf.Deg2Rad * alpha);
			leftAngle = (Mathf.Atan (wheelBaseLength / (xc + (wheelBaseWidth / 2)))) * Mathf.Rad2Deg;
			rightAngle = (Mathf.Atan (wheelBaseLength / (xc - (wheelBaseWidth / 2)))) * Mathf.Rad2Deg;

			//leftAngle = Mathf.Atan2 (wheelBaseLength * orientation, x - wheelBaseWidth / 2 * orientation) * Mathf.Rad2Deg;
			//rightAngle = Mathf.Atan2 (wheelBaseLength * orientation, x + wheelBaseWidth / 2 * orientation) * Mathf.Rad2Deg;
			//	print (leftAngle + ", " + rightAngle);


			//Visual debugging turn point
			
			Vector3 top = (WheelLF.transform.position + WheelRF.transform.position) / 2;
			Vector3 bot = (WheelLB.transform.position + WheelRB.transform.position) / 2;
			Vector3 turn = bot + transform.right * x * orientation;
			Debug.DrawLine (top, turn, Color.green);
			Debug.DrawLine (bot, turn, Color.yellow);
			Debug.DrawRay (WheelLF.transform.position, alpha / Mathf.Abs (alpha) * WheelLF.transform.right * 20, Color.red); //left angle
			Debug.DrawRay (WheelRF.transform.position, alpha / Mathf.Abs (alpha) * WheelRF.transform.right * 20, Color.red); //right angle
			Vector3 turnPoint = bot + WheelLB.right * x;
		}
		//print ("a: " + alpha + ", b: " + leftAngle + ", c:" + rightAngle);
		//slows down the rotation
		ColliderLF.steerAngle = Mathf.Lerp (ColliderLF.steerAngle, leftAngle, 0.2f);
		ColliderRF.steerAngle = Mathf.Lerp (ColliderRF.steerAngle, rightAngle, 0.2f);
	}

	private void EulerRotations (float degrees, float leftAngle, float rightAngle)
	{

		//Euler rotation speed
		if (true) {
			WheelLB.Rotate (new Vector3 (degrees, 0, 0));
			WheelRB.Rotate (new Vector3 (degrees, 0, 0));
		}
		//Euler turn
		if (!combined) {
		
			WheelLF.localEulerAngles = (new Vector3 (WheelLF.localEulerAngles.x, leftAngle, 0));
			WheelRF.localEulerAngles = (new Vector3 (WheelRF.localEulerAngles.x, rightAngle, 0));
		} else {
			//combination just not easily possible
			WheelLF.localEulerAngles = (new Vector3 (WheelLF.localEulerAngles.x, leftAngle, WheelLF.localEulerAngles.z));
			WheelRF.localEulerAngles = (new Vector3 (WheelRF.localEulerAngles.x, rightAngle, WheelRF.localEulerAngles.z));
			WheelLF.Rotate (new Vector3 (degrees, 0, 0));
			WheelRF.Rotate (new Vector3 (degrees, 0, 0));

		}

	}

	private void QuaternionsRotations (float degrees, float leftAngle, float rightAngle)
	{

		Quaternion spinning = Quaternion.AngleAxis (degrees, Vector3.Cross (transform.up, transform.forward));

		if (true) {
			WheelLB.rotation = spinning * WheelLB.rotation;
			WheelRB.rotation = spinning * WheelRB.rotation;
		}
		if (!combined) {
			WheelLF.localRotation = Quaternion.AngleAxis (leftAngle, transform.up);
			WheelRF.localRotation = Quaternion.AngleAxis (rightAngle, transform.up);
		} else { 
			//combined using quaternions, possible
			Quaternion l = Quaternion.AngleAxis (leftAngle, transform.up);
			Quaternion r = Quaternion.AngleAxis (rightAngle, transform.up);
			WheelLF.rotation = l * spinning * WheelLB.rotation;
			WheelRF.rotation = r * spinning * WheelRB.rotation;
		}
	}

}
