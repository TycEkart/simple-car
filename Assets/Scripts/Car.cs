using UnityEngine;
using System.Collections;

public class Car : MonoBehaviour
{
		//Colliders, these are the actual wheels that make the car move.
		public WheelCollider ColliderLF;
		public WheelCollider ColliderRF;
		public WheelCollider ColliderLB;
		public WheelCollider ColliderRB;
		//These are just fancy looks, we need to make them turn and rotate
		public Transform WheelLF;
		public Transform WheelRF;
		public Transform WheelLB;
		public Transform WheelRB;
		//you want to boost your vehicle or make it unstable, change these variables
		private const float MAX_STEER = 30; 		//[degrees]
		private const float MAX_TORQUE = 200; 		//[Nm]
		//Used for debugging and showing progress to the teacher
		public bool eulerTesting = true;
		public bool combined = false;
		public float wheelBaseLength;
		public float wheelBaseWidth;
		// Use this for initialization
		void Start ()
		{
				//to keep the car from flipping lower the center of mass
				rigidbody.centerOfMass -= Vector3.up * 0.5f;
				wheelBaseWidth = Vector3.Distance (WheelLF.position, WheelRF.position);
				wheelBaseLength = Vector3.Distance (WheelLF.position, WheelLB.position);
		}
	
		// Update is called once per frame
		void Update ()
		{
				// Visual debuging using DrawRay and DrawLine, thank me later
				Debug.DrawRay (transform.position + transform.up, transform.forward, Color.blue);
				Debug.DrawRay (transform.position + transform.up, transform.up, Color.green);
				Debug.DrawRay (transform.position + transform.up, Vector3.Cross (transform.up, transform.forward), Color.red);

				//input from keyboard / joystick
				MoveForwardReverse (Input.GetAxis ("Vertical") * MAX_TORQUE);
				//calculate the angle of both front wheels using Ackermann steering geometry.
				SetWheelColliderAngles (Input.GetAxis ("Horizontal") * MAX_STEER);

				/*	
				 * 1. Calculate the correct spinning angle, tip: Wheelcolliders have a RPM parameter
				 */
				float currentSpinAngle = 0;

				if (eulerTesting) {
						/*	
						 * 2. Aply rotations using eulerangles only!
						 */
						EulerRotations (currentSpinAngle, ColliderLF.steerAngle, ColliderRF.steerAngle);
				} else {
						/*	
						 * 3. Aply rotations using quaternions.
						 */
						QuaternionsRotations (currentSpinAngle, ColliderLF.steerAngle, ColliderRF.steerAngle);
				}

		}


		//Puts negative or positive torque on the car to move the car
		private void MoveForwardReverse (float moveValue)
		{
				if (moveValue > 0) {
						ColliderLB.motorTorque = moveValue;
						ColliderRB.motorTorque = moveValue;
				} else if (moveValue < 0) {
						ColliderLB.motorTorque = moveValue;
						ColliderRB.motorTorque = moveValue;
				} else {
						ColliderRB.motorTorque = 0;
						ColliderLB.motorTorque = 0;
				}
	
		}

		private void SetWheelColliderAngles (float alpha)
		{
				float leftAngle = 0;
				float rightAngle = 0;
				if (alpha != 0) {
						/*
						 * 4. 	Calculate the ackerman angles for each wheel
						 * tip: http://i258.photobucket.com/albums/hh264/fjcamper/Turncenter.gif
						 */ 
						leftAngle = alpha;
						rightAngle = alpha;
					
				}
				//slows down the rotation
				ColliderLF.steerAngle = Mathf.Lerp (ColliderLF.steerAngle, leftAngle, 0.2f);
				ColliderRF.steerAngle = Mathf.Lerp (ColliderRF.steerAngle, rightAngle, 0.2f);
		}

		private void EulerRotations (float degrees, float leftAngle, float rightAngle)
		{
				/*	
				 * 2. Aply rotations using Eulerangles only!
				 */
				{
						/*	
				 		 * 2.a  Rotate the back wheels
						 */
				}
				//Euler turn
				if (!combined) {
						/*	
				 		 * 2.b  turn the front wheels (left / right)
						 */
				} else {
						/*	
				 		 * 2.c  turn the front wheels (left / right)
				 		 * 		rotate the frontwheels
				 		 * 
				 		 * tip: correct order in rotation matters!
						 */
				}

		}

		private void QuaternionsRotations (float degrees, float leftAngle, float rightAngle)
		{
				/*	
				 * 3. Aply rotations using Quaternions only!
				 */
				{
						/*	
				 		 * 3.a  Rotate the back wheels
						 */
				}
				if (!combined) {
						/*	
				 		 * 3.b  turn the front wheels (left / right)
						 */
				} else { 
						/*	
				 		 * 3.c  turn the front wheels (left / right)
				 		 * 		rotate the frontwheels
				 		 * 
				 		 * tip: correct order in rotation matters!
						 */
				}
		}

}
